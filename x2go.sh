#!/bin/bash

yum install epel-release
yum groupinstall "Xfce"
#startx
systemctl set-default graphical

sudo dnf config-manager --set-enabled PowerTools
sudo dnf install x2goserver

reboot
