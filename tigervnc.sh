#!/bin/bash

sudo dnf groupinstall "Server with GUI" -y
sudo dnf install tigervnc-server -y

#To connect to the server via VNC, it is better not to use the root user, because it is not safe. In addition, some functions may not work correctly. Therefore, we will create a regular user for connection, set a password for it and add it to the wheel group. This will give him sudo authority.

#adduser 
#passwd username
#usermod -aG wheel username

#Now we need to assign each user an individual port for connection. To do this, open the file:

usr=$(whoami)
line=":2=$usr"
sudo chown -R $usr /etc/tigervnc/vncserver.users

#Add the following line to the end. You can put as many users with ports there as you need. The last digit of the port is shown here. The first part will always be 590x. In this case, we will set port 5902 for the user username. The port for each user must be unique.
sudo echo $line >> /etc/tigervnc/vncserver.users

#Now open the default config file.
sudo chown -R $usr /etc/tigervnc/vncserver-config-defaults

#Add the default configuration here:
sudo echo "session=gnome" >> /etc/tigervnc/vncserver-config-defaults

#Now you need to log in with the newly created user.
#su - username

#Set a password for this TigerVNC server user. If you choose a view-only password, you won’t be able to control your computer, only watch.

#vncpasswd

#Now start the TigerVNC service and add it to autostart.
sudo systemctl enable --now vncserver@:2

#https://serverspace.io/support/help/install-tigervnc-server-on-centos-8/
