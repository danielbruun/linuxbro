CentOS
2 cpu
256 min RAM
40GB

Ved Hyper-V -> Set secure boot. UEFI authority certificate
Set ip om ikke DC


dnf update -y

#Graphics
 #KDE
#dnf --enablerepo=epel,powertools group -y install "KDE Plasma Workspaces" "base-x"
#systemctl set-default graphical
#reboot
#startx

#Gnome
dnf groupinstall "Server with GUI" -y

sudo dnf install epel-release -y

#XRDP
sudo dnf install xrdp -y
sudo systemctl enable xrdp --now
sudo systemctl restart xrdp

#Firewall
# Må også åpnes i Azure om det brukes. Security group. Må også ha user, og passw
sudo dnf install firewalld -y
sudo systemctl restart firewalld 

sudo firewall-cmd --new-zone=xrdp --permanent
sudo firewall-cmd --zone=xrdp --add-port=3389/tcp --permanent
#sudo firewall-cmd --zone=xrdp --add-source=192.168.1.0/24 --permanent
sudo firewall-cmd --reload

#KEymap
localectl set-keymap no