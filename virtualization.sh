#!/bin/bash

#KVM
dnf module install virt
dnf install virt-install virt-viewer libguestfs-tools 
#sudo dnf -y install virt-top libguestfs-tools #Virtualization tools
systemctl enable libvirtd.service
systemctl start libvirtd.service
systemctl status libvirtd.service  | grep Active:
#sudo yum -y install virt-manager #GUI for virtualization

    #Network bridge
    #nmcli connection show #List available connections
    #ip link show type bridge
    
    #Variables for bridge
    BR_NAME="createdBridge"
    BR_INT="virbr0"
    SUBNET_IP="192.168.122.10/24"
    GW="192.168.122.1"
    DNS1="8.8.8.8"
    DNS2="8.8.4.4"

    #Define new bridge connection
    sudo nmcli connection add type bridge autoconnect yes con-name $BR_NAME ifname $BR_NAME

    #Modify bridge IP,GW and DNS
    sudo nmcli connection modify $BR_NAME ipv4.addresses $SUBNET_IP ipv4.method manual
    sudo nmcli connection modify $BR_NAME ipv4.gateway $GW
    sudo nmcli connection modify $BR_NAME ipv4.dns $DNS1 +ipv4.dns $DNS2
    
    #Add network device as bridge slave
    sudo nmcli connection delete $BR_INT
    sudo nmcli connection add type bridge-slave autoconnect yes con-name $BR_INT ifname $BR_INT master $BR_NAME

    #Check connection
    #sudo nmcli connection show 

    #Bring up network bridge
    sudo nmcli connection up $BR_NAME

#Create VM on KVM
    #Download Centos8 ISO, and check
    sudo cd /var/lib/libvirt/boot/
    sudo wget -O centos8.iso http://ftp.heanet.ie/pub/centos/8-stream/isos/x86_64/CentOS-Stream-8-x86_64-20210506-boot.iso

    sudo wget http://ftp.heanet.ie/pub/centos/8-stream/isos/x86_64/CHECKSUM
    sudo sha256sum --ignore-missing -c CHECKSUM

    if [ $? -eq 0 ]
    then 
        echo CHECKSUM OK 
    else 
        echo CHECKSUM FAILED
        exit
    fi
    

virt-install \
--virt-type=kvm \
--name centos8-vm \
--memory 2048 \
--vcpus=1 \
--os-variant=rhel8.1 \
--cdrom=/var/lib/libvirt/boot/centos8.iso \
--network=bridge=$BR_NAME,model=virtio \
--graphics vnc \
--disk path=/var/lib/libvirt/images/centos8.qcow2,size=20,bus=virtio,format=qcow2