#!/bin/bash

wget https://download.nomachine.com/download/7.1/Linux/nomachine_7.1.3_1_x86_64.rpm
dnf install nomachine_7.1.3_1_x86_64.rpm

#Firewall
firewall-cmd --add-port=4000/tcp --permanent
firewall-cmd --add-port=4011-4999/udp --permanent
firewall-cmd --reload

