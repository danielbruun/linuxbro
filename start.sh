#!/bin/bash

#bash >(curl https://bitbucket.org/danielbruun/linuxbro/raw/HEAD/start.sh)
#curl https://bitbucket.org/danielbruun/linuxbro/raw/HEAD/start.sh | bash

# ssh -L 13389:[Windows Server RDP address]:3389 [address ssh server] -l [ssh username] -N
# 127.0.0.2:3388:

#Graphics
 #KDE
#dnf --enablerepo=epel,powertools group -y install "KDE Plasma Workspaces" "base-x"
#systemctl set-default graphical
#reboot
#startx

#Gnome
sudo dnf groupinstall "Server with GUI" -y

sudo dnf install epel-release -y

#XRDP
#sudo dnf install xrdp -y
#sudo systemctl enable xrdp --now
#sudo systemctl restart xrdp
# systemctl status xrdp

#Xfce
sudo dnf groupinstall "Xfce" -y
#startx
sudo systemctl set-default graphical

#x2go
sudo yum config-manager --set-enabled powertools
sudo yum install x2goserver -y

    #Change x2go hotkey for terminating session
    sed -i 's/key="t"/key="y"/g' /etc/x2go/keystrokes.cfg

#Firewall
# Må også åpnes i Azure om det brukes. Security group. Må også ha user, og passw
sudo dnf install firewalld -y
sudo systemctl restart firewalld 

sudo firewall-cmd --new-zone=xrdp --permanent
sudo firewall-cmd --zone=xrdp --add-port=3389/tcp --permanent
#sudo firewall-cmd --zone=xrdp --add-source=192.168.1.0/24 --permanent
sudo firewall-cmd --reload

#KEymap
sudo localectl set-keymap no

#Dconf custom keybinding

sudo bash -c "cat << EOF > /etc/dconf/db/local.d/01-Terminal

#dconf path
[org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0]

binding='<Primary><Alt>t'
command='gnome-terminal'
name='terminal'
EOF"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"

sudo dconf update

#Install vsCode
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

sudo bash -c "cat << EOF > /etc/yum.repos.d/vscode.repo
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF"

sudo dnf install code -y

#Install Git and Bitwarden
sudo dnf install git -y

sudo dnf install snapd -y
sudo systemctl enable --now snapd.socket
sudo snap install bitwarden
#Give snap time to init
sudo snap install bitwarden

sudo dnf update -y

git clone https://danielbruun@bitbucket.org/danielbruun/linuxbro.git

#Reboot, for GUI
sudo reboot

